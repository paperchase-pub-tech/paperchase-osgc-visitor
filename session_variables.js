if (Meteor.isClient) {
    Session.setDefault('advance', null);
    // altmetrics badge
    Session.setDefault('altmetric-ready', false);
    Session.setDefault('altmetric-top', null);
    Session.setDefault('altmetric-count', 50);
    Session.setDefault('article-altmetric', null); // for single article
    Session.setDefault('archive',null);
    // Article
    Session.setDefault('articleData',null);
    Session.setDefault('article-id',null);// Article Overview, Article Full Text, Article Purchase
    Session.setDefault('article-list',null);// for section papers list
    Session.setDefault('article-text',null);
    Session.setDefault('article-text-modified',null);
    Session.setDefault('conferences', false);
    Session.setDefault('current',null);
    // Editorial Board
    Session.setDefault('edBoardEic', null);
    Session.setDefault('edBoardBoard', null);
    Session.setDefault('edBoardNew', null);
    // Error
    Session.setDefault('error',false);
    Session.setDefault('errorMessages',null);
    // interview
    Session.setDefault('latest-interview', null);
    // Issue
    Session.setDefault('issue',null);
    Session.setDefault('issueParams',null);
    Session.setDefault('navSection',null);
    Session.setDefault('processing', false); // for form
    // Search
    Session.setDefault('search-loaded', null);
    Session.setDefault('search-loading', null);
    Session.setDefault('query-results', null);
    Session.setDefault('search-args', {
        general: '',
        authors: null,
        abstract: null,
        title: null,
        keywords: null,
        agingSearch: false,
        oncotargetSearch: false,
        genesandcancerSearch: false,
        oncoscienceSearch: false
    });
}
