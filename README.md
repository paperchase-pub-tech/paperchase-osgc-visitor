**Paperchase Documentation**
========================
An academic journal platform.

## Pages & Data
### Ethics
This is a 'section page'. Section data is stored in the ```ethics``` collection, order of sections is stored in the ```sorters``` collection.
### For Authors
This is a 'section page'. Section data is stored in the ```for_authors``` collection, order of sections is stored in the ```sorters``` collection. There is also additional text at the top of the page, which is stored in the ```config``` collection, ```site.for_authors_top```.
