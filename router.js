/**
 * Router
 *
 */
var subs = new SubsManager();

if (Meteor.isClient) {
    Router.route('/', {
        name: 'Home',
        layoutTemplate: 'MainLayout',
        waitOn: function(){
            return[
                Meteor.subscribe('homePagePublic'),
                Meteor.subscribe('sortedList','homePage'),
                Meteor.subscribe('articlesFeature'),
                Meteor.subscribe('newsListDisplay'),
                Meteor.subscribe('issueCurrent'),
                Meteor.subscribe('issuePublishedCurrent'),
                Meteor.subscribe('interviewMostRecent')
            ];
        },
        data: function(){
            if(this.ready()){
                var featureList = articles.find({'feature':true},{sort:{'_id':1}}).fetch();

                var sortedHomePage  = sorters.findOne({name : 'homePage'});
                var homePageSections = sortedHomePage && sortedHomePage.ordered ? sortedHomePage.ordered : null;

                var mostRecentInterview = newsList.findOne({display:true, interview:true},{sort : {date: -1}});

                return {
                    feature : featureList,
                    news:  newsList.find({display:true, interview:false},{sort : {date: -1}}).fetch(),
                    sections : homePageSections,
                    interview : mostRecentInterview
                };
            }
        },
        onAfterAction: function() {
            if (!Meteor.isClient) {
                return;
            }
            var title = Meteor.settings.public.journal.name;

            var journal = Session.get('journal');

            if (journal && journal.site && journal.site.meta && journal.site.meta.home && journal.site.meta.home.title){
                title += ' | ' + journal.site.meta.home.title;
            }

            var meta = Meteor.homePage.meta();

            SEO.set({
                title: title,
                meta: meta
            });
        }
    });

    Router.route('/about', {
        name: 'About',
        layoutTemplate: 'MainLayout',
        onBeforeAction: function(){


            Meteor.call('getAbout', function(error, result){
                if (result) {
                    Session.set('about', result);
                }
            });

            this.next();
        },
        onAfterAction: function() {
            if (!Meteor.isClient) {
                return;
            }
            var title = Meteor.settings.public.journal.name + ' | About the Journal';

            SEO.set({
                title: title
            });
        }
    });

    Router.route('/account', {
        name: 'Account',
        layoutTemplate: 'MainLayout',
        onBeforeAction: function(){

            this.next();
        },
        onAfterAction: function() {
            if (!Meteor.isClient) {
                return;
            }
            var title = Meteor.settings.public.journal.name + ' | Account';

            SEO.set({
                title: title
            });
        }
    });

    Router.route('/advance', {
        name: 'Advance',
        layoutTemplate: 'MainLayout',
        waitOn: function(){
            return[
                subs.subscribe('advance'),
                subs.subscribe('sortedList', 'advance')
            ];
        },
        onAfterAction: function() {
            if (!Meteor.isClient) {
                return;
            }
            var title = Meteor.settings.public.journal.name + ' | Advance Articles';

            SEO.set({
                title: title
            });
        },
        data: function(){
            var advanceSorter, articlesList;
            if (this.ready()){
                advanceSorter = sorters.findOne({ name: 'advance' });
                if (advanceSorter && advanceSorter.ordered) {
                    articlesList = advanceSorter.ordered;
                    console.log('articlesList',articlesList);
                }
                return {
                    articles: articlesList
                };
            }
        }
    });

    Router.route('/archive', {
        name: 'Archive',
        layoutTemplate: 'MainLayout',
        title: 'Issue Archive',
        showLink: true,
        onBeforeAction: function(){
            Meteor.call('archive',function(error,result){
                if(error){
                    console.error('Archive Error', error);
                }else if(result){
                    Session.set('archive',result);
                }
            });
            this.next();
        },
        waitOn: function(){
            return[
                subs.subscribe('volumes'),
            ];
        },
        onAfterAction: function() {
            if (!Meteor.isClient) {
                return;
            }
            var title = Meteor.settings.public.journal.name + ' | Archive';
            var meta = Meteor.archive.meta();

            SEO.set({
                title: title,
                meta: meta
            });
        }
    });

    Router.route('/article/:id', {
        name: 'Article',
        parent: 'Issue',
        layoutTemplate: 'MainLayout',
        showLink: true,
        title: function() {
            var title = Meteor.settings.public.journal.name;

            if (Session.get('article')) {
                meta = Meteor.article.metaTags(Session.get('article'), false);

                if (Session.get('article').title) {
                    title += ' | ' +  Meteor.article.pageTitle(Session.get('article').title, null);
                }
            }

            return title;
        },
        onBeforeAction: function(){
            Meteor.article.readyFullText(this.params.id);

            if (Session.get('article')) {
                if (Session.get('article')._id === this.params.id && Session.get('article').ids && Session.get('article').ids.pii) {
                    Router.go('/article/' + Session.get('article').ids.pii);
                }  // redirect to PII route
            }

            if (Session.get('article')) {
                Session.set('article-id', this.params.id);

                // Redirect if not set to display
                if (!Session.get('article').display){
                    Router.go('ArticleNotFound');
                }
            }

            this.next();
        },
        waitOn: function(){
            subs.subscribe('articleByEitherId', this.params.id);
        },
        onAfterAction: function() {
            if (!Meteor.isClient) {
                return;
            }
            var meta = {};
            var title = Meteor.settings.public.journal.name;

            if (Session.get('article')) {
                meta = Meteor.article.metaTags(Session.get('article'), false);

                if (Session.get('article').title) {
                    title += ' | ' +  Meteor.article.pageTitle(Session.get('article').title, null);
                }
            }

            SEO.set({
                title: title,
                meta: meta
            });
        }
    });

    Router.route('/article/:id/text', {
        name: 'ArticleText',
        parent: 'Issue',
        showLink: true,
        layoutTemplate: 'MainLayout',
        title: function() {
            var title = Meteor.settings.public.journal.name;

            if (Session.get('article')) {
                meta = Meteor.article.metaTags(Session.get('article'), false);

                if (Session.get('article').title) {
                    title += ' | ' +  Meteor.article.pageTitle(Session.get('article').title, null);
                }
            }

            return title;
        },
        waitOn: function(){
            subs.subscribe('articleByEitherId', this.params.id);
        },
        onBeforeAction: function() {
            Meteor.article.readyFullText(this.params.id);

            if (Session.get('article')) {
                if (Session.get('article')._id === this.params.id && Session.get('article').ids && Session.get('article').ids.pii) {
                    Router.go('/article/' + Session.get('article').ids.pii);
                }  // redirect to PII route
            }

            if (Session.get('article')) {
                Session.set('article-id', this.params.id);

                // Redirect if not set to display
                if (!Session.get('article').display){
                    Router.go('ArticleNotFound');
                }
            }

            this.next();
        },
        onAfterAction: function() {
            if (!Meteor.isClient) {
                return;
            }
            var meta = {};
            var title = Meteor.settings.public.journal.name;

            if (Session.get('article')) {
                meta = Meteor.article.metaTags(Session.get('article'), true);

                if (Session.get('article').title) {
                    title += ' | ' +  Meteor.article.pageTitle(Session.get('article').title, ' - Full Text');
                }
            }

            SEO.set({
                title: title,
                meta: meta
            });
        }
    });

    Router.route('/article/:id/figures', {
        name: 'ArticleFigures',
        parent: 'Article',
        layoutTemplate: 'MainLayout',
        showLink: true,
        title: function() {
            var title = Meteor.settings.public.journal.name;

            if (Session.get('article')) {
                meta = Meteor.article.metaTags(Session.get('article'), false);

                if (Session.get('article').title) {
                    title += ' | ' +  Meteor.article.pageTitle(Session.get('article').title, null);
                }
            }

            return title;
        },
        waitOn: function(){
            subs.subscribe('articleByEitherId', this.params.id);
        },
        onBeforeAction: function() {
            Meteor.article.readyFullText(this.params.id);

            if (Session.get('article')) {
                if (Session.get('article')._id === this.params.id && Session.get('article').ids && Session.get('article').ids.pii) {
                    Router.go('/article/' + Session.get('article').ids.pii);
                }  // redirect to PII route
            }

            if (Session.get('article')) {
                Session.set('article-id', this.params.id);

                // Redirect if not set to display
                if (!Session.get('article').display){
                    Router.go('ArticleNotFound');
                }
            }

            this.next();
        },
        onAfterAction: function() {
            if (!Meteor.isClient) {
                return;
            }
            var meta = {};
            var title = Meteor.settings.public.journal.name;

            if (Session.get('article')) {
                meta = Meteor.article.metaTags(Session.get('article'), true);

                if (Session.get('article').title) {
                    title += ' | ' +  Meteor.article.pageTitle(Session.get('article').title, ' - Figures');
                }
            }

            SEO.set({
                title: title,
                meta: meta
            });
        }
    });

    Router.route('/article/:article/figures/:figureId', {
        name: 'ArticleFigureViewer',
        layoutTemplate: 'ArticleFigureViewer',
        waitOn: function(){

            return[
                Meteor.subscribe('articleByEitherId', this.params.article),
                Meteor.subscribe('articleTypes')
            ];
        },
        data: function(){
            if(this.ready()){
                var article,
                    figure;
                var articleId = this.params.article;
                var figureId = this.params.figureId;
                article = articles.findOne({'ids.pii': articleId});

                if (article) {
                    article = Meteor.article.readyData(article);

                    Session.set('article',article);
                    Session.set('article-id',articleId);

                    if(article && figureId && article.files && article.files.figures){
                        article.files.figures.forEach(function(fig){
                            if(fig.id.toLowerCase() === figureId.toLowerCase()){
                                figure = fig;
                            }
                        });
                    } else if(articleId){
                        Router.go('Article',{_id : articleId});
                    }

                    if(!figure && articleId){
                        Router.go('Article',{_id : articleId});
                    }
                } else {
                    Router.go('ArticleNotFound');
                }

                return {
                    article: article,
                    figure: figure
                };
            }
        },
        onBeforeAction: function() {
            // check if article exists
            var articleExistsExists = articles.findOne({'ids.pii': this.params.article});
            // Redirect if not set to display
            if(!articleExistsExists){
                Router.go('ArticleNotFound');
            } else if (!articleExistsExists.display){
                Router.go('ArticleNotFound');
            } else{
                Meteor.article.altmetric(articleExistsExists);
            }

            Session.set('article-id',this.params.article);

            this.next();
        },
        onAfterAction: function() {
            if (!Meteor.isClient) {
                return;
            }
            var articleId = this.params.query.article;
            var meta = {};
            var title = Meteor.settings.public.journal.name;
            var doi;

            if (this.data() && this.data().article) {
                if (this.data().article.ids && this.data().article.ids.doi){
                    doi = this.data().article.ids.doi.replace('http://dx.doi.org/', '');
                    meta.citation_doi = 'doi:' + doi;
                }

                if (this.data().article.title) {
                    title += ' | ' +  Meteor.article.pageTitle(this.data().article.title, ' - Figure');
                }
            }

            SEO.set({
                title: title,
                meta: meta
            });
        }
    });

    Router.route('/conferences', {
        name: 'Conferences',
        layoutTemplate: 'MainLayout',
        waitOn: function(){
            return [
                Meteor.subscribe('issueCurrent')
            ];
        },
        onBeforeAction: function(){

            Meteor.call('conferencesPastAndFuture', function(error, result){
                if (error) {
                    console.error(error);
                } else {
                    Session.set('conferences', result);
                }
            });
            this.next();
        },
        onAfterAction: function() {
            if (!Meteor.isClient) {
                return;
            }
            var title = Meteor.settings.public.journal.name + ' | Conferences';

            SEO.set({
                title: title
            });
        }
    });

    Router.route('/conference-request', {
        name: 'ConferenceRequest',
        layoutTemplate: 'MainLayout',
        title: 'Conference Request',
        onAfterAction: function() {
            if (!Meteor.isClient) {
                return;
            }
            var title = Meteor.settings.public.journal.name + ' | Conference Request';

            SEO.set({
                title: title
            });
        },
        data: function() {
            var text = '';
            var journal = Session.get('journal');
            if (journal && journal.site && journal.site.conference && journal.site.conference.text) {
                text  = journal.site.conference.text;
            }

            return {
                text: text
            };
        }
    });

    Router.route('/conference-request-received', {
        name: 'ConferenceRequestComplete',
        layoutTemplate: 'MainLayout',
        title: 'Conference Request Received',
        onAfterAction: function() {
            if (!Meteor.isClient) {
                return;
            }
            var title = Meteor.settings.public.journal.name + ' | Conference Request Received';

            SEO.set({
                title: title
            });
        }
    });

    Router.route('/contact', {
        name: 'Contact',
        layoutTemplate: 'MainLayout',
        waitOn: function(){
            return [
                subs.subscribe('contact')
            ];
        },
        onAfterAction: function() {
            if (!Meteor.isClient) {
                return;
            }
            var title = Meteor.settings.public.journal.name + ' | Contact';

            SEO.set({
                title: title
            });
        },
        data: function(){
            var contactData;
            if (this.ready()){
                contactData = contact.findOne();
                return {
                    contact: contactData
                };
            }
        }
    });

    Router.route('/current', {
        name: 'Current',
        onBeforeAction: function(){
            if(Session.get('current')){
                Router.go('Issue', {vi: Session.get('current').params});
            }
        }
    });

    Router.route('/editorial-board', {
        name: 'EdBoard',
        layoutTemplate: 'MainLayout',
        onAfterAction: function() {
            if (!Meteor.isClient) {
                return;
            }
            var title = Meteor.settings.public.journal.name + ' | Editorial Board';

            SEO.set({
                title: title
            });
        }
    });

    Router.route('/ethics', {
        name: 'Ethics',
        layoutTemplate: 'MainLayout',
        waitOn: function() {
            return [
                subs.subscribe('ethicsPublic'),
                subs.subscribe('sortedList', 'ethics')
            ];
        },
        onAfterAction: function() {
            if (!Meteor.isClient) {
                return;
            }
            var title = Meteor.settings.public.journal.name + ' | Publication Ethics and Publication Malpractice Statements';

            SEO.set({
                title: title
            });
        },
        data: function(){
            var ethicsSorter, ethicsSections;
            if(this.ready()){
                ethicsSorter = sorters.findOne({ name:'ethics' });
                if (ethicsSorter && ethicsSorter.ordered){
                    ethicsSections = ethicsSorter.ordered;
                }

                return {
                    sections: ethicsSections
                };
            }
        }
    });

    Router.route('/for-authors', {
        name: 'ForAuthors',
        layoutTemplate: 'MainLayout',
        showLink: true,
        title: 'For Authors',
        waitOn: function() {
            return [
                subs.subscribe('forAuthorsPublic'),
                subs.subscribe('sortedList', 'forAuthors')
            ];
        },
        onAfterAction: function() {
            if (!Meteor.isClient) {
                return;
            }
            var title = Meteor.settings.public.journal.name + ' | For Authors';

            SEO.set({
                title: title
            });
        },
        data: function(){
            var journal, forAuthorsSorter, forAuthorsSections, forAuthorsTop;
            if(this.ready()){
                journal = journalConfig.findOne();
                // Top Text
                forAuthorsTop =  journal && journal.site && journal.site.for_authors_top ? journal.site.for_authors_top : null;

                // Sections
                forAuthorsSorter = sorters.findOne({ name:'forAuthors' });
                if (forAuthorsSorter && forAuthorsSorter.ordered){
                    forAuthorsSections = forAuthorsSorter.ordered;
                }

                return {
                    top: forAuthorsTop,
                    sections: forAuthorsSections
                };
            }
        }
    });

    Router.route('/interviews', {
        name: 'Interviews',
        layoutTemplate: 'MainLayout',
        onBeforeAction: function(){

            this.next();
        },
        waitOn: function() {
            return [
                subs.subscribe('interviews')
            ];
        },
        data: function() {
            return {
                interviews: newsList.find({display: true, interview: true},{sort: {'date':-1}}).fetch()
            };
        },
        onAfterAction: function() {
            if (!Meteor.isClient) {
                return;
            }
            var title = Meteor.settings.public.journal.name + ' | Interviews';

            SEO.set({
                title: title
            });
        }
    });

    Router.route('/issue/:vi', {
        name: 'Issue',
        parent: 'Archive',
        layoutTemplate: 'MainLayout',
        showLink: true,
        title: function() {
            var pageTitle = '';
            var pieces = Meteor.issue.urlPieces(this.params.vi);

            if(Session.get('journal')){
                pageTitle = Session.get('journal').journal.name + ' | ';
            }

            if(pieces && pieces.volume){
                pageTitle += 'Volume ' + pieces.volume + ', Issue ' + pieces.issue;
            }

            return pageTitle;
        },
        waitOn: function() {
            var pieces = Meteor.issue.urlPieces(this.params.vi);
            if (pieces && pieces.volume && pieces.issue){
                return [
                    subs.subscribe('issuePublished', pieces.volume, pieces.issue )
                ];
            }
        },
        onBeforeAction: function(){
            // TODO: add redirect if no issue
            var pieces = Meteor.issue.urlPieces(this.params.vi);

            if(pieces && pieces.volume){
                Session.set('issueParams', {volume: parseInt(pieces.volume), issue: pieces.issue.toString()});
            }

            this.next();
        },
        onAfterAction: function() {
            var meta = {};
            if (!Meteor.isClient) {
                return;
            }
            var pieces = Meteor.issue.urlPieces(this.params.vi);
            var title = Meteor.settings.public.journal.name;

            if (this.ready()) {
                var issue = publish.findOne({publication_type: 'issue', volume: parseInt(pieces.volume), issue_linkable: pieces.issue.toString()});
                if (issue) {
                    meta = Meteor.issue.meta(issue.data);
                } else {
                    Router.go('IssueNotFound');
                }

                if(pieces && pieces.volume){
                    title += ' | Volume ' + pieces.volume + ', Issue ' + pieces.issue;
                }
            }

            SEO.set({
                title: title,
                meta: meta
            });
        },
        data: function(){
            var pieces = Meteor.issue.urlPieces(this.params.vi);
            if (this.ready() && pieces && pieces.volume && pieces.issue) {
                return {
                    issueData: Meteor.issue.getPublishedIssueAndFiles(pieces.volume, pieces.issue)
                };
            }
        }
    });

    Router.route('/print-request/:ids', {
        name: 'PrintRequest',
        layoutTemplate: 'MainLayout',
        title: 'Print Request',
        showLink: true,
        onBeforeAction: function(){


            this.next();
        },
        waitOn: function(){
            var ids = this.params.ids;
            if(ids.substring(0,1) == 'v') {
                ids = [];
            } else {
                ids = ids.split(',');
            }
            return[
                Meteor.subscribe('articlesById',ids),
            ];
        },
        data: function(){
            var article = {};
            if(this.ready()){
                var ids = this.params.ids;
                if(ids.substring(0,1) == 'v') {
                    ids = ids.substring(1).split('i');
                    article.titles = ["Vol "+ids[0]+", Iss "+ids[1]];
                    article.urls = ["http://aging-us.com/issue/"+this.params.ids];
                }
                else {
                    ids = ids.split(',');
                    var arts = articles.find({'_id': {"$in":ids}});
                    if(arts){
                        article.titles = [];
                        article.piis = [];
                        article.urls = [];
                        arts.forEach(function(art) {
                            article.titles.push(Meteor.clean.stripHtml(art.title));
                            article.piis.push(art.ids.pii);
                            article.urls.push("http://www.aging-us.com/article/"+art._id);
                        });
                    }
                }

                article.titles = article.titles.join('\n\r');

                return {
                    article: article
                };
            }
        }
    });

    Router.route('/print-request-complete', {
        name: 'PrintRequestComplete',
        layoutTemplate: 'MainLayout'
    });

    Router.route('/section/:_section_dash_name', {
        name: 'SectionPapers',
        layoutTemplate: 'MainLayout',
        onBeforeAction: function(){

            Meteor.call('preprocessSectionArticles',articles.find().fetch(), function(error,result){
                if(error){
                    console.log('ERROR - preprocessSectionArticles');
                    console.log(error);
                }
                if(result){
                    Session.set('article-list',result);
                }
            });
            this.next();
        },
        waitOn: function(){

            return [
                Meteor.subscribe('sectionPapersByDashName', this.params._section_dash_name)
            ];
        },
        data: function(){
            if(this.ready()){
                // TODO: this route forever loads. Currently it is not in use
                // more data set in helpersData.js, articles
                return {
                    section : sections.findOne({dash_name : this.params._section_dash_name})
                };
            }
        },
        onAfterAction: function() {
            if (!Meteor.isClient) {
                return;
            }
            var title = Meteor.settings.public.journal.name;

            var section = sections.findOne({dash_name : this.params._section_dash_name});
            if (section) {
                title += ' | ' + section.name;
            }

            SEO.set({
                title: title
            });
        }
    });

    Router.route('/search', {
        name: 'Search',
        layoutTemplate: 'MainLayout',
        onAfterAction: function(e) {
            var title = Meteor.settings.public.journal.name + ' | Search';

            SEO.set({
                title: title
            });
        },
        waitOn: function(){
            return[
                Meteor.subscribe('journalConfig')
            ];
        },
        data: function() {
            if(this.ready()){
                var journal = journalConfig.findOne();
                var indexes, primaryIndex;
                var anyIndexSelected = false;

                // For the search index (journal) checkboxes
                if (journal && journal.elasticsearch && journal.elasticsearch.indexes) {
                    indexes = journal.elasticsearch.indexes;
                    if (Session.get('search-args')) {
                        indexes.forEach(function(ind){
                            if (Session.get('search-args')[ind.index + 'Search'] === true) {
                                ind.selected = true;
                                anyIndexSelected = true;
                            }
                        });
                    }
                }

                if (!anyIndexSelected && journal && journal.elasticsearch && journal.elasticsearch.primaryIndex) {
                    // default is the current journal index, unless it was unselected by user
                    primaryIndex = journal.elasticsearch.primaryIndex;
                    indexes.forEach(function(ind){
                        if (ind.index === primaryIndex) {
                            ind.selected = true;
                        }
                    });
                }

                return {
                    indexes: indexes
                };
            }
        }
    });

    Router.route('/subscribe', {
        name: 'Subscribe',
        layoutTemplate: 'MainLayout',
        onBeforeAction: function(){

            this.next();
        },
        waitOn: function(){
            return[
                Meteor.subscribe('issueCurrent')
            ];
        },
        data: function(){
            if(this.ready()){
                return{
                    issue: issues.findOne(),
                    today: new Date(),
                    nextYear: new Date(new Date().setYear(new Date().getFullYear() + 1))
                };
            }
        },
        onAfterAction: function() {
            if (!Meteor.isClient) {
                return;
            }
            var title = Meteor.settings.public.journal.name + ' | Subscribe';

            SEO.set({
                title: title
            });
        }
    });

    Router.route('/top-articles', {
        name: 'TopArticles',
        layoutTemplate: 'MainLayout',
        onBeforeAction: function(){

            this.next();
        },
        waitOn: function(){

            return[
                Meteor.subscribe('issueCurrent'),
            ];
        },
        title: function() {
            var pageTitle = '';
            if(Session.get('journal')){
                pageTitle = Session.get('journal').journal.name + ' | ';
            }
            return pageTitle + 'Top Articles';
        },
    });

    // 404
    Router.route('/404/article', {
        name: 'ArticleNotFound',
        layoutTemplate: 'MainLayout',
        onBeforeAction: function(){
            this.next();
        },
        onAfterAction: function() {
            if (!Meteor.isClient) {
                return;
            }
            var title = Meteor.settings.public.journal.name + ' | 404: Article Not Found';

            SEO.set({
                title: title
            });
        }
    });

    Router.route('/404/issue', {
        name: 'IssueNotFound',
        layoutTemplate: 'MainLayout',
        onBeforeAction: function(){

            this.next();
        },
        onAfterAction: function() {
            if (!Meteor.isClient) {
                return;
            }
            var title = Meteor.settings.public.journal.name + ' | 404: Issue Not Found';

            SEO.set({
                title: title
            });
        }
    });
}
