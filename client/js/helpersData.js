if (Meteor.isClient) {
    Template.About.helpers({
        sections: function() {
            if(Session.get('about')){
                return Session.get('about');
            }
        }
    });

    Template.Archive.helpers({
        volumes: function(){
            return Session.get('archive');
        }
    });

    Template.Article.helpers({
        article:function() {
            return Session.get('article');
        },
        fullText: function(){
            return Session.get('article-text');
        }
    });

    Template.ArticleFigure.helpers({
        articleId: function() {
            return Session.get('article-id');
        }
    });

    Template.ArticleText.helpers({
        article:function() {
            return Session.get('article');
        },
        fullText: function(){
            return Session.get('article-text');
        }
    });

    Template.ArticleFigures.helpers({
        article:function() {
            return Session.get('article');
        }
    });

    Template.AuthorAffsAndNotes.helpers({
        tooltipAffiliation: function() {
            var articleData = Template.parentData(2);
            var authorData = Template.parentData(1);
            var num = parseInt(this);
            if(articleData.affiliations) {
                return articleData.affiliations[num];
            }
            else {
                return '';
            }
        }
    });

    Template.Conferences.helpers({
        past: function() {
            return Session.get('conferences') &&  Session.get('conferences').past ? Session.get('conferences').past : false;
        },
        future: function() {
            return Session.get('conferences') &&  Session.get('conferences').future ? Session.get('conferences').future : false;
        }
    });

    Template.EdBoard.helpers({
        eic: function() {
            if(Session.get('edBoardEic')){
                return Session.get('edBoardEic');
            }

        },
        board: function() {
            if(Session.get('edBoardBoard')){
                return Session.get('edBoardBoard');
            }
        }
    });

    Template.ErrorMessages.helpers({
        errors: function(){
            return Session.get('errorMessages');
        }
    });

    Template.Footer.helpers({
        publisher : function(){
            var journalSettings = journalConfig.findOne();
            if(journalSettings){
                return journalSettings.journal.publisher.name;
            }
        },
        issn : function(){
            var journalSettings = journalConfig.findOne();
            if(journalSettings){
                return journalSettings.journal.issn;
            }
        },
        year: function(){
            return new Date().getFullYear();
        }
    });

    Template.Home.helpers({
        altmetricTop: function() {
            // only list first 10 on homepage
            if (Session.get('altmetric-top')) {
                return Meteor.general.getFirstXFromArray(5, Session.get('altmetric-top'));
            } else{
                return;
            }
        },
        altmetricTopCount: function() {
            return Meteor.general.numberToWord(Session.get('altmetric-count'));
        },
        journal: function() {
            return Meteor.settings.public.journal.name;
        },
        slides: function(){
            var journal = Session.get('journal');
            return journal && journal.site && journal.site.home_slideshow? journal.site.home_slideshow : null;
        }
    });

    Template.Loading.helpers({
        issueParams: function(){
            return Session.get('issueParams');
        }
    });

    Template.MainLayout.helpers({
        nameExtra: function(){
            return Meteor.settings.public.journal.nameExtra;
        },
        configLoaded: function (){
            var journalSettings = Session.get('journal');
            if(journalSettings){
                return true;
            }
        }
    });

    Template.PrintRequest.helpers({
        submitted: function(){
            return Session.get('submitted');
        }
    });

    Template.Search.helpers({
        searchLoading: function(){
            return Session.get('search-loading');
        },
        searchLoaded: function() {
            return Session.get('search-loaded');
        },
        queryResults: function() {
            return Session.get('query-results');
        }
    });

    Template.SectionPapers.helpers({
        articles: function(){
            return Session.get('article-list');
        }
    });

    Template.SearchForm.helpers({
        searchArgs: function(){
            return Session.get('search-args');
        }
    });

    Template.SideBar.helpers({
        latestIssue: function() {
            return Session.get('current');
        },
        latestInterview: function() {
            return Session.get('latest-interview');
        }
    });

    Template.SubscribeModal.helpers({
        article: function(){
            return Session.get('articleData');
        }
    });

    Template.TopArticles.helpers({
        altmetricTop: function() {
            return Session.get('altmetric-top');
        },
        altmetricTopCount: function() {
            return Meteor.general.numberToWord(Session.get('altmetric-count'));
        }
    });
}
