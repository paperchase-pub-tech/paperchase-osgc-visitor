Template.registerHelper('affiliationNumber', function(affiliation) {
    if (affiliation == '*') return affiliation;
    return parseInt(parseInt(affiliation) + 1);
});
Template.registerHelper('articleId', function() {
    return Session.get('article-id');
});
Template.registerHelper('prettyDoi', function(doi) {
    return doi.replace('http://dx.doi.org/', '');
});
// Article References
Template.registerHelper('punctuationCheck', function(string) {
    if (string.charAt(string.length - 1) != '.' && string.charAt(string.length - 1) != '?' ){
        return '.';
    } else {
        return;
    }
});
Template.registerHelper('referenceColonCheck', function(ref) {
    var result;
    var reference = ref.hash.reference;
    if (reference && reference.volume && reference.issue || reference.comment || reference.fpage){
        result = ':';
    }
    return result;
});
Template.registerHelper('referenceSemicolonCheck', function(ref) {
    var result;
    var reference = ref.hash.reference;
    if (reference && reference.volume && reference.issue || reference.fpage){
        result = ';';
    }
    return result;
});
// Article Dates
Template.registerHelper('articleDate',function(date) {
    if(date){
        return Meteor.dates.article(date);
    }
    return;
});
Template.registerHelper('collectionDate',function(date) {
    if(date){
        return moment(date).format('MMMM YYYY');
    }
    return;
});
Template.registerHelper('formatDate', function(date) {
    if (date) {
        return Meteor.dates.wordDate(date);
    }
    return;
});
Template.registerHelper('formatDateSpan', function(dateStart, dateEnd) {
    if ( dateStart && dateEnd ) {
        return Meteor.dates.dateSpan( dateStart, dateEnd );
    }
    return;
});
Template.registerHelper('formatIssueDate',function(date,settings) {
    if(date){
        if(settings.day && settings.month && settings.year){
            return moment(date).format('d MMMM, YYYY');
        }else if(settings.month && settings.year){
            return moment(date).format('MMMM YYYY');
        }else if(settings.day && settings.year){
            return moment(date).format('d YYYY'); //TODO prevent day being selected if no month
        }else if(settings.day && settings.month){
            return moment(date).format('Do MMMM');
        }else if(settings.year){
            return moment(date).format('YYYY');
        }
    }
    return;
});
