if (Meteor.isClient) {
    // Advance -------
    Template.Advance.events({
        'click .modal-trigger': function(e) {
            Meteor.article.subscribeModal(e);
        },
        'click .download-pdf': function(e) {
            Meteor.article.downloadPdf(e);
        }
    });

    // Article -------
    Template.Article.events({
        'click .modal-trigger': function(e) {
            Meteor.article.subscribeModal(e);
        }
    });
    Template.ArticleLinks.events({
        'click .download-pdf': function(e){
            Meteor.googleAnalytics.sendEvent('Full Text - PDF',e);
        },
        'click .view-html': function(e){
            Meteor.googleAnalytics.sendEvent('Full Text - HTML',e);
        }
    });
    Template.ArticleFullText.events({
        'click .anchor': function(e) {
            Meteor.general.scrollAnchor(e);
        }
    });
    Template.ArticleFigureViewer.events({
        'click .figure-back-button': function(e,t) {
            window.history.back();
        }
    });
    Template.ArticleKeywords.events({
        'click .search-kw': function(e, t) {
            e.preventDefault();
            var keyword = $(e.target).text();
            keyword = keyword ? Meteor.clean.stripHtml(keyword) : keyword;
            if (keyword) {
                Meteor.search.setSearchArg('general', keyword);
            }
            Router.go('Search');
        }
    });

    // Conference Request ----
    Template.ConferenceRequestForm.events({
        'submit #conference-request': function(e, t) {
            e.preventDefault();
            // Reset error message for email. We add an invalid message if email did not pass validation, so if for was submitted twice, reset attr.
            $('label[for=email]').attr('data-error', 'Email is Required');

            var form = {};
            var emptyFields = [];
            var validEmail = false;
            var inputOffset;
            var navHeight = Meteor.general.navHeight(); // for scrolling to

            form.first_name = e.target.first_name.value ? e.target.first_name.value : '';
            form.last_name = e.target.last_name.value ? e.target.last_name.value : '';
            form.conference_title = e.target.conference_title.value ? e.target.conference_title.value : '';
            form.organization = e.target.organization.value ? e.target.organization.value : '';
            form.email = e.target.email.value ? e.target.email.value : '';
            form.phone = e.target.phone.value ? e.target.phone.value : '';
            form.conference_topic = e.target.conference_topic.value ? e.target.conference_topic.value : '';
            form.conference_date_start = e.target.conference_date_start.value ? e.target.conference_date_start.value : '';
            form.conference_date_end = e.target.conference_date_end.value ? e.target.conference_date_end.value : '';
            form.conf_description = e.target.conf_description.value ? e.target.conf_description.value : '';

            validEmail = Meteor.form.validateEmail(form.email);

            for (var key in form) {
                if (form[key].replace(/ /g,'') === '') {
                    emptyFields.push(key);
                }
            }

            // Empty inputs (minus whitespace) or Invalid Email
            if (emptyFields.length > 0 || !validEmail) {
                // Empty Input
                emptyFields.forEach(function(inputId){
                    $('#' + inputId).addClass('invalid');
                });

                // Invalid email
                if (!validEmail && form.email !== '') {
                    // Only use invalid message if input not empty.
                    $('label[for=email]').attr('data-error', 'Email is Not Valid');
                    $('#email').addClass('invalid');
                }

                // scroll to first
                if (emptyFields[0]) {
                    inputOffset = $('#' + emptyFields[0]).offset().top;
                    inputOffset = inputOffset - navHeight;
                    Meteor.general.scrollToPosition(inputOffset);
                } else {
                    inputOffset = $('#email').offset().top;
                    inputOffset = inputOffset - navHeight;
                    Meteor.general.scrollToPosition(inputOffset);
                }

            } else {
                Session.set('processing', true);
                // Form is all valid
                Meteor.call('emailConferenceRequest', form, function(error, result){
                    if (error) {
                        console.error('emailConferenceRequest', error);
                    } else {
                        Router.go('ConferenceRequestComplete');
                    }
                });
            }
        }
    });

    // EdBoard  ------------
    Template.EdBoard.events({
        'click .modal-trigger': function(e){
            e.preventDefault();
        }
    });

    // Footer ------
    Template.Footer.events({
        'click .previous-site-link': function(e){
            ga('send', 'event', 'Archive Site');
        }
    });

    // For Authors
    // -------
    Template.ForAuthors.events({
        'click .anchor': function(e) {
            Meteor.general.scrollAnchor(e);
        }
    });

    // Header -------
    Template.Header.events({
        'click .searchbox-button': function(e){
            e.preventDefault();
            var generalTerm = $('.searchbox-field').val();
            Meteor.search.bounceTo(generalTerm);
        },
        'keypress .searchbox-field': function (e, template) {
            if (e.which === 13) {
                var generalTerm = $('.searchbox-field').val();
                Meteor.search.bounceTo(generalTerm);
            }
        }
    });

    // Home ------------
    Template.Home.events({
        'click .view-all-top-home': function(e){
            Meteor.googleAnalytics.sendEvent('View All - Via Homepage',e);
        }
    });

    // Interviews -----
    Template.Interviews.events({
        'click .interview-tag': function(e){
            e.preventDefault();
            var keyword = $(e.target).text();
            keyword = keyword ? Meteor.clean.stripHtml(keyword) : keyword;
            if (keyword) {
                Meteor.search.setSearchArg('general', keyword);
            }
            Router.go('Search');
        }
    });

    // Issue -------
    Template.Issue.events({
        'click .modal-trigger': function(e) {
            Meteor.article.subscribeModal(e);
        },
        'click .anchor': function(e) {
            Meteor.general.scrollAnchor(e);
        },
        'click .issue-change': function(e) {
            Session.set('issue',null);
            Session.set('issueParams',null);
        }
    });

    // News ------------
    Template.LatestNews.events({
        'click .almetric-news-link': function(e){
            Meteor.googleAnalytics.sendEvent('Almetric News Link',e);
        }
    });

    // Print Request -------
    Template.PrintRequest.events({
        'submit #print-request': function(e,t) {
            e.preventDefault();
            var target = e.target;
            var from = target.email.value;
            var pii = target.pii.value.replace(/(^\s*,)|(,\s*$)/g, '');
            var url = target.url.value;
            var title = target.title.value;
            var name = target.name.value;
            var phone = target.phone.value;
            var institution = target.institution.value;
            var shippingAddress = target.shippingAddress.value;
            var specs = target.specs.value;
            var number = target.number.value;
            var color = target.color.checked ? 'Color' : 'B/W';
            var supplements = target.supplements.checked ? 'Yes' : 'No';
            var cover = target.cover.checked ? 'Yes' : 'No';

            var text = '<b>Name:</b> '+name+' <br><b>Email:</b> '+from+' <br><b>Additional information / special instructions:</b> '+' <p>'+specs+'</p>'+' <br><b>Article Title(s):</b> '+'<p>'+title+'</p>'+' <br><b>Article Pii:</b> ' +pii+' <br><b>Article URLs:</b> ' +url+' <br><b>Phone Number:</b> '+phone+' <br><b>Institution:</b> '+institution+' <br><b>Shipping Address:</b> '+shippingAddress+' <br><b>Number of Copies:</b> '+number+' <br><b>Color/Black & White:</b> '+color+' <br><b>Include cover?:</b> '+cover+' <br><b>Include Supplementary Material?:</b> '+supplements;

            Meteor.call('sendPrintRequestEmail', text, function(error, result){
                if (result) {
                    Router.go('PrintRequestComplete');
                }
            });
        }
    });

    // Recommend -------
    Template.Recommend.events({
        'submit form': function(e,t){
            e.preventDefault();
            Meteor.formActions.saving();
            var inputs = {};
            var errors = [];

            inputs.user_id = $('#user_id').val();
            inputs.name_first = $('#name_first_input').val();
            inputs.user_email = $('#email_input').val();
            inputs.name_last = $('#name_last_input').val();
            inputs.institution =  $('#institution_input').val();
            inputs.position =  $('#position_input').val();
            inputs.lib_email = $('#lib_email_input').val();
            inputs.message =  $('#message_input').val();

            //get checkboxe recommendations
            var recommendations = [];
            $('.checkbox-recommend').each(function(i){
                if($(this).prop('checked')){
                    var checkbox_id = $(this).attr('id');
                    var text = $('label[for="'+checkbox_id+'"]').text();
                    recommendations.push(text);
                }
            });
            if(recommendations.length > 0){
                inputs.recommendations = recommendations;
            }


            // check required
            if(!inputs.institution){
                errors.push('Institution' + ' Required');
                $('#institution_input').addClass('invalid');
            }

            if(errors.length === 0 ){
                //submit form
                Meteor.call('addRecommendation', inputs, function(error,result){
                    if(error){
                        console.log('ERROR - addRecommendation');
                        console.log(error);
                    }else{
                        Meteor.formActions.success();
                    }
                });
            }else{
                //show errors
                Meteor.formActions.error(); //update dom for user
                Session.set('errorMessages',errors); //reactive template variable for ErrorMessages will loop through these
            }
        }
    });

    // Search ------
    Template.Search.events({
        'submit #search-form': function(e) {
            e.preventDefault();
            Meteor.search.clearVariables();
            var searchArgs = {
                general: (e && e.target && e.target.general) ? e.target.general.value : generalTerm,
                authors: (e && e.target && e.target.authors) ? e.target.authors.value : null,
                abstract: (e && e.target && e.target.abstract) ? e.target.abstract.value : null,
                title: (e && e.target && e.target.title) ? e.target.title.value : null,
                keywords: (e && e.target && e.target.keywords) ? e.target.keywords.value : null,
                agingSearch: (e && e.target && e.target.agingSearch && e.target.agingSearch.checked) ? true : false,
                oncotargetSearch: (e && e.target && e.target.oncotargetSearch && e.target.oncotargetSearch.checked) ? true : false,
                genesandcancerSearch: (e && e.target && e.target.genesandcancerSearch && e.target.genesandcancerSearch.checked) ? true : false,
                oncoscienceSearch: (e && e.target && e.target.oncoscienceSearch && e.target.oncoscienceSearch.checked) ? true : false
            };

            Session.set('search-args', searchArgs);

            Meteor.search.searchLoad();
        }
    });

    // Section -------
    Template.SectionPapers.events({
        'click .anchor': function(e) {
            Meteor.general.scrollAnchor(e);
        }
    });

    // Subscribe -------
    Template.Subscribe.events({
        'click button': function(e){
            e.preventDefault();
        }
    });

    // TOC ----
    Template.tocCover.events({
        'click .cover-modal-trigger': function(e){
            e.preventDefault();
        }
    });
}
