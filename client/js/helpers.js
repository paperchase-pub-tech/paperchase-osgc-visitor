if (Meteor.isClient) {
    // -- Altmetric
    Template.registerHelper('altmetricReady', function() {
        return Session.get('altmetric-ready');
    });
    Template.registerHelper('altmetricTemplate', function() {
        var journal = Session.get('journal');
        return journal && journal.altmetric && journal.altmetric.template ? journal.altmetric.template : null;
    });
    Template.registerHelper('altmetricThreshold', function() {
        var journal = Session.get('journal');
        return journal && journal.altmetric && journal.altmetric.threshold ? journal.altmetric.threshold : null;
    });
    Template.registerHelper('altmetricDetailsLink', function() {
        var journal = Session.get('journal');
        return journal && journal.altmetric && journal.altmetric.report_link ? journal.altmetric.report_link : null;
    });

    // -- Editorial Board
    Template.registerHelper('edBoardEic', function(){
        return Session.get('edBoardEic');
    });
    Template.registerHelper('edBoardNew', function(){
        return Session.get('edBoardNew');
    });
    Template.registerHelper('edBoard', function(){
        return Session.get('edBoard');
    });

    // -- General
    Template.registerHelper('anchorName', function(string){
        return string.replace(/\s/g,'');
    });
    Template.registerHelper('arrayify',function(obj){
        result = [];
        for (var key in obj) {
            result.push({name:key,value:obj[key]});
        }
        return result;
    });
    Template.registerHelper('checked', function(bool) {
        if (bool) {
            return 'checked';
        }
    });
    Template.registerHelper('emailSearchHelp', function(){
        var journal = Session.get('journal');
        if (journal && journal.site && journal.site.emails && journal.site.emails.search_help) {
            return journal.site.emails.search_help;
        }
    });
    Template.registerHelper('equals', function (a, b) {
        return a == b;
    });
    Template.registerHelper('journalName', function() {
        var journal = Meteor.settings.public && Meteor.settings.public.journal && Meteor.settings.public.journal.name ? Meteor.settings.public.journal.name : '';
        return journal;
    });
    Template.registerHelper('mailchimpTOC', function() {
        var journal = Session.get('journal');
        if (journal && journal.site && journal.site.mailchimp && journal.site.mailchimp.toc) {
            return journal.site.mailchimp.toc;
        }
    });
    Template.registerHelper('notEmpty', function (a) {
        if(!a){
            return false;
        }else if(a.length > 0){
            return true;
        }else{
            return false;
        }
    });
    Template.registerHelper('authorNotEmpty', function(authorObj){
        if (authorObj && authorObj.name_first !== '' || authorObj.name_middle !== '' ||  authorObj.name_last !== '') {
            if (authorObj.name_first || authorObj.name_middle ||  authorObj.name_last) {
                return true;
            }
        }
    });
    Template.registerHelper('pluralCheck', function(array){
        if (array.length > 1) {
            return true;
        }
        return false;
    });
    Template.registerHelper('processing', function(){
        return Session.get('processing');
    });
    Template.registerHelper('removeLink', function(str) {
        return str.replace(/<a\b[^>]*>/i,'').replace(/<\/a>/i, '');
    });
    Template.registerHelper('truncate', function(str, limit) {
        if ( str.length > limit + 3 ) {
            str = str.substr( 0, limit ) + "...";
        }
        return str;
    });
    Template.registerHelper('wrapInParagraphTag', function(str){
        if(str === undefined) return '';
        if(str.indexOf('<p>') === -1){
            return '<p>' + str + '</p>';
        }else{
            return str;
        }
    });
    Template.registerHelper('removePunctuation',function(string){
        return Meteor.clean.removeEndPeriod(string);
    });

    // -- Article
    Template.registerHelper('oa', function(){
        return true;
    });

    // --  News
    Template.registerHelper('conferenceDateCheckPass', function(item){
        if (item && item.conference_date_start){
            if (item.conference_date_start < new Date()){
                return false;
            }
        }
        return true;
    });
    Template.registerHelper('notEmptyNews', function (a) {
        var anyPass = 0;
        if(!a){
            return false;
        }else if(a.length > 0){
            a.forEach(function(item){
                if (item && item.conference_date_start){
                    if (item.conference_date_start < new Date()){
                        return false;
                    } else {
                        anyPass++;
                    }
                } else{
                    anyPass++;
                }
            });
            if (anyPass > 0) {
                return true;
            } else {
                return false;
            }
        }else{
            return false;
        }
    });

    // -- Journal specific styling
    Template.registerHelper('journalClass', function(){
        var journal = Session.get('journal');
        var journalClass = journal && journal.site && journal.site.style && journal.site.style.main_class ? journal.site.style.main_class : null;
        return journalClass;
    });

    // - Journal From DB
    Template.registerHelper('submitLink', function(){
        var journal = Session.get('journal');
        var link = journal && journal.submission && journal.submission.url ? journal.submission.url : null;
        return link;
    });
}
