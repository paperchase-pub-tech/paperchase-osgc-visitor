Template.AltmetricBadgeWithReport.onRendered(function() {
    _altmetric_embed_init();
});

Template.Article.onRendered(function() {
    window.prerenderReady = true;
});

Template.ArticleHeader.onRendered(function() {
    window.prerenderReady = true;
});

Template.ArticleFigureViewerFig.onRendered(function() {
    $('.figure img, .table img').wrap('<div class="container"></div>');
    var viewerOptions = {
        $zoomIn: $('.zoom-in'),
        $zoomOut: $('.zoom-out'),
        $zoomRange: $('.zoom-range'),
        $reset: $(".reset"),
        maxScale: 3,
        increment: 0.1
    };

    var $panzoom = $('.figure img, .table img').panzoom(viewerOptions).panzoom('zoom', true);
});

Template.ArticleHeader.onRendered(function() {
    window.prerenderReady = true;
});

Template.ArticleText.onRendered(function() {
    window.prerenderReady = true;
});

Template.bioModal.onRendered(function() {
  $('.modal').modal(); // bio modals
});

Template.ConferenceRequest.onRendered(function(){
    Session.set('processing', false);
});

Template.ConferenceRequestComplete.onRendered(function(){
    Session.set('processing', false);
});

Template.ConferenceRequestForm.onRendered(function(){
    $('.datepicker').pickadate({});
});

Template.EdBoardList.onRendered(function() {
    $('.collapsible').collapsible();
});

Template.Issue.onDestroyed(function() {
    Session.set('issueParams', null);
});

Template.Loading.onRendered(function() {
    if(Router.current().route.getName() === 'Issue' && Router.current().params && Router.current().params.vi){
        var pieces = Meteor.issue.urlPieces(Router.current().params.vi);

        if(pieces && pieces.volume){
            Session.set('issueParams', {volume: parseInt(pieces.volume), issue: pieces.issue.toString()});
        }
    }
});

Template.MainLayout.onRendered(function () {
    $('.button-collapse').sideNav();
    window.prerenderReady = true;
});

Template.PrintRequest.onRendered(function(){
    Materialize.updateTextFields();
    $('select').material_select();
    $('#title').trigger('autoresize');
});

Template.Search.onRendered(function () {
    Meteor.search.searchLoad();
});

Template.SectionPapers.onRendered(function () {
    Session.set('article-list',null);
});

Template.Slideshow.onRendered(function() {
    $('.slideshow').responsiveSlides({
        timeout: 6000,
        pause: true,
        pager: true
    });
});

Template.Subscribe.onRendered(function () {
    $('select').material_select();
});

Template.tocCover.onRendered(function() {
    $('.modal').modal();
});
