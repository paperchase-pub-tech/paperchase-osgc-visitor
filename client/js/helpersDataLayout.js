if (Meteor.isClient) {
    Template.Header.helpers({
        issn: function(){
            var journal = Session.get('journal');
            return journal && journal.journal && journal.journal.issn ? journal.journal.issn : null;
        }
    });

    Template.HeaderSocial.helpers({
        socialList: function(){
            var journal = Session.get('journal');
            return journal && journal.site && journal.site.social ? journal.site.social : null;
        }
    });

    Template.Nav.helpers({
        links: function(){
            var journal = Session.get('journal');
            return journal && journal.site && journal.site.side_nav ? journal.site.side_nav : null;
        },
        twitter: function(){
            var journal = Session.get('journal');
            return journal && journal.site && journal.site.twitter ? journal.site.twitter : null;
        }
    });
}
