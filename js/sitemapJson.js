/*
    For manually setting data for sitemap. Otherwise, URL data autogenerated via gadicohen:sitemaps
*/

sitemapJson = {
    oncoscience : {
        urlset: {
            url: []
        }
    }
};
