about = new Mongo.Collection('about');
articles = new Mongo.Collection('articles', {
    transform: function(f){
        if(f.ids.doi && _.isString(f.ids.doi)) {
            f.ids.doi = f.ids.doi.replace(/http:\/\/dx\.doi\.org\//,"");
        }
        return f;
    }
});
articleTypes = new Mongo.Collection('article_types');
authors = new Mongo.Collection('authors');
contact = new Mongo.Collection('contact');
edboard = new Mongo.Collection('edboard');
ethics = new Mongo.Collection('ethics');
forAuthors = new Mongo.Collection('for_authors');
homePage = new Mongo.Collection('homePage');
issues = new Mongo.Collection('issues', {
    transform: function(f) {
        if (f.volume && f.issue){
            f.param = Meteor.issue.createIssueParam(f.volume, f.issue);
        }

        return f;
    }
});
journalConfig = new Mongo.Collection('config');
newsList = new Mongo.Collection('news');
publish = new Mongo.Collection('publish');
recommendations = new Mongo.Collection('recommendations');
sections = new Mongo.Collection('sections', {
    transform: function(f) {
        if (f.special) {
            // for special collections
            var journal =  journalConfig.findOne();
            if (journal && journal.assets) {
                f.optimized_urls = {
                    small:  journal.assets+'special_collections_covers/'+f._id+'-sm.gif',
                    medium:  journal.assets+'special_collections_covers/'+f._id+'-md.gif',
                    large:  journal.assets+'special_collections_covers/'+f._id+'-lg.gif'
                };
                f.pdfPath = journal.assets + 'special_collections_pdf/' + f._id + '.pdf';
                f.epubPath = journal.assets + 'special_collections_epub/' + f._id + '.epub';
                f.coverPath = journal.assets + 'special_collections_covers/' + f._id + '.png';
                f.largeCover = true;
            }
            if (f.name) {
                f.cover_alt = f.name;
            }
        }
        return f;
    }
});
sorters = new Mongo.Collection('sorters', {
    transform: function(f) {
        var unordered,
            ordered;
            var order = f.order;

        if(f.name == 'advance'){
            unordered = articles.find({'_id':{'$in':order}}).fetch();
            ordered = Meteor.sorter.sort(unordered,order);
            ordered = Meteor.organize.groupArticles(ordered);
        } else if(f.name == 'ethics'){
            unordered = ethics.find({'_id':{'$in':order}}).fetch();
        } else if(f.name == 'homePage'){
            unordered = homePage.find({'_id':{'$in':order}}).fetch();
        }else if(f.name == 'forAuthors'){
            unordered = forAuthors.find({'_id':{'$in':order}}).fetch();
        }else if(f.name == 'about'){
            unordered = about.find({'_id':{'$in':order}}).fetch();
        }else if(f.name == 'sections'){
            unordered = sections.find({'_id':{'$in':order}}).fetch();
        }

        if (unordered && f.name !== 'advance') {
            // advance will have ordered set above, because need to group articles by type
            ordered = Meteor.sorter.sort(unordered, order);
        }

        if (ordered) {
            f.ordered = ordered;
        }

        return f;
    }
});
volumes = new Mongo.Collection('volumes');

var issueFieldsDeny = {fields: {doc_updates: 0, previous:0}};
var articleFieldsDeny = {fields: {doc_updates: 0, previous:0, submissions:0}};

// ALLOW
if (Meteor.isServer) {
    recommendations.allow({
        insert: function (userId, doc, fields, modifier) {
            return true;
        },
        update: function (userId, doc, fields, modifier) {
            var u = Meteor.users.findOne({_id:userId});
            if (Roles.userIsInRole(u, ['super-admin'])) {
                return true;
            }
        },
        remove: function (userId, doc, fields, modifier) {
            var u = Meteor.users.findOne({_id:userId});
            if (Roles.userIsInRole(u, ['super-admin'])) {
                return true;
            }
        }
    });
}
// DENY
if (Meteor.isServer) {
    Meteor.users.deny({
        insert: function (userId, doc, fields, modifier) {
            return true;
        },
        update: function (userId, doc, fields, modifier) {
            return false;
        },
        remove: function (userId, doc, fields, modifier) {
            return false;
        }
    });

    about.deny({
        insert: function (userId, doc, fields, modifier) {
            return true;
        },
        update: function (userId, doc, fields, modifier) {
            return true;
        },
        remove: function (userId, doc, fields, modifier) {
            return true;
        }
    });

    articles.deny({
        insert: function (userId, doc, fields, modifier) {
            return true;
        },
        update: function (userId, doc, fields, modifier) {
            return true;
        },
        remove: function (userId, doc, fields, modifier) {
            return true;
        }
    });

    articleTypes.deny({
        insert: function (userId, doc, fields, modifier) {
            return true;
        },
        update: function (userId, doc, fields, modifier) {
            return true;
        },
        remove: function (userId, doc, fields, modifier) {
            return true;
        }
    });

    authors.deny({
        insert: function (userId, doc, fields, modifier) {
            return true;
        },
        update: function (userId, doc, fields, modifier) {
            return true;
        },
        remove: function (userId, doc, fields, modifier) {
            return true;
        }
    });

    edboard.deny({
        insert: function (userId, doc, fields, modifier) {
            return true;
        },
        update: function (userId, doc, fields, modifier) {
            return true;
        },
        remove: function (userId, doc, fields, modifier) {
            return true;
        }
    });

    forAuthors.deny({
        insert: function (userId, doc, fields, modifier) {
            return true;
        },
        update: function (userId, doc, fields, modifier) {
            return true;
        },
        remove: function (userId, doc, fields, modifier) {
            return true;
        }
    });

    issues.deny({
        insert: function (userId, doc, fields, modifier) {
            return true;
        },
        update: function (userId, doc, fields, modifier) {
            return true;
        },
        remove: function (userId, doc, fields, modifier) {
            return true;
        }
    });

    journalConfig.deny({
        insert: function (userId, doc, fields, modifier) {
            return true;
        },
        update: function (userId, doc, fields, modifier) {
            return true;
        },
        remove: function (userId, doc, fields, modifier) {
            return true;
        }
    });

    newsList.deny({
        insert: function (userId, doc, fields, modifier) {
            return true;
        },
        update: function (userId, doc, fields, modifier) {
            return true;
        },
        remove: function (userId, doc, fields, modifier) {
            return true;
        }
    });

    sections.deny({
        insert: function (userId, doc, fields, modifier) {
            return true;
        },
        update: function (userId, doc, fields, modifier) {
            return true;
        },
        remove: function (userId, doc, fields, modifier) {
            return true;
        }
    });

    sorters.deny({
        insert: function (userId, doc, fields, modifier) {
            return true;
        },
        update: function (userId, doc, fields, modifier) {
            return true;
        },
        remove: function (userId, doc, fields, modifier) {
            return true;
        }
    });

    volumes.deny({
        insert: function (userId, doc, fields, modifier) {
            return true;
        },
        update: function (userId, doc, fields, modifier) {
            return true;
        },
        remove: function (userId, doc, fields, modifier) {
            return true;
        }
    });
}

// PUBLISH
if (Meteor.isServer) {
    Meteor.publish(null, function (){
        return Meteor.roles.find({});
    });

    Meteor.publish('aboutPublic', function(){
        // TODO: not currently in use, route uses method, but will switch route to use this
        return about.find({display:true});
    });

    Meteor.publish('advance', function () {
        var articlesList;
        var sorter = sorters.findOne({'name':'advance'});
        if (sorter && sorter.order) {
            articlesList = articles.find({'_id':{'$in':sorter.order}}, articleFieldsDeny);
        }

        return articlesList;
    });

    Meteor.publish('articleByEitherId', function (articleId) {
        return articles.find({ $or: [ { _id: articleId }, { 'ids.pii': articleId } ] }, articleFieldsDeny);
    });

    Meteor.publish('articlesById', function(pii) {
        check(pii, Array);
        return articles.find({'_id': {"$in":pii}}, articleFieldsDeny);
    });

    Meteor.publish('articlesFeature', function () {
        return articles.find({'feature':true},{sort:{'_id':1}}, articleFieldsDeny);
    });

    Meteor.publish('articleTypes', function () {
        return articleTypes.find({},{});
    });

    Meteor.publish('contact', function() {
        return contact.find();
    });

    Meteor.publish('eb', function () {
        return edboard.find({'role': {'$in': ['Founding Editorial Board']}});
    });

    Meteor.publish('eic', function () {
        return edboard.find({'role': {'$in': ['Editor-in-Chief']}});
    });

    Meteor.publish('ethicsPublic', function(){
        return ethics.find({display:true});
    });

    Meteor.publish('forAuthorsPublic', function(){
        return forAuthors.find({display:true});
    });

    Meteor.publish('homePagePublic', function(){
        return homePage.find({display:true});
    });

    Meteor.publish('interviewMostRecent', function(){
        return newsList.find({display: true, interview: true});
    });

    Meteor.publish('interviews', function(){
        return newsList.find({display: true, interview: true});
    });

    Meteor.publish('issueCurrent',function(){
        return issues.find({current: true}, issueFieldsDeny);
    });

    Meteor.publish('issuePublished', function (volume, issue) {
        return publish.find({'publication_type':'issue','volume':parseInt(volume), 'issue_linkable':issue});
    });

    Meteor.publish('issuePublishedCurrent', function () {
        var result = [];
        var current = issues.findOne({current: true});
        if (current) {
            result = publish.find({'publication_type':'issue','volume':parseInt(current.volume), 'issue_linkable':current.issue});
        }
        return result;
    });

    Meteor.publish('issues', function () {
        return issues.find({display : true},{sort : {volume:-1,issue:1}},{volume:1,issue:1,pub_date:1});
    });

    Meteor.publish('journalConfig', function() {
        var siteConfig =  journalConfig.find({},{fields: {
            'altmetric': 1,
            'assets': 1,
            'assets_figures': 1,
            'assets_supplemental': 1,
            'edboard_roles' : 1,
            'elasticsearch': 1,
            'journal' : 1,
            's3': 1,
            'site' : 1,
            'submission.url' : 1,
            'social': 1,
            'visitor': 1
        }});
        return siteConfig;
    });

    Meteor.publish('newsListDisplay', function(){
        return newsList.find({display: true, interview: false});
    });

    Meteor.publish('sectionByDashName', function(dashName){
        check(dashName, String);
        return sections.find({dash_name : dashName});
    });

    Meteor.publish('sectionPapers', function(sectionMongoId){
        check(sectionMongoId, String);
        return articles.find({'section' : sectionMongoId});
    });

    Meteor.publish('sectionPapersByDashName', function(dashName){
        check(dashName, String);
        var section = sections.findOne({'dash_name' : dashName});
        return articles.find({'section' : section._id});
    });

    Meteor.publish('sectionsVisible', function(){
        return sections.find({display: true});
    });

    Meteor.publish('sortedList', function(listName) {
        check(listName, String);
        return sorters.find({'name' : listName});
    });

    Meteor.publish('sorters', function() {
        return sorters.find();
    });

    Meteor.publish('volumes', function () {
        return volumes.find({},{sort : {volume:-1}});
    });
}
