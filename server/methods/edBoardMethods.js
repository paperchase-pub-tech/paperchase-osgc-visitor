Meteor.methods({
    getEic: function(){
        return edboard.find({role:'Editor-in-Chief'},{sort : {name_last:1}}).fetch();
    },
    getBoardMinusEic: function() {
        return edboard.find({role : {$ne : 'Editor-in-Chief'}},{sort : {name_last:1}}).fetch();
    },
    getBoardNew: function() {
        return edboard.find({new: true},{sort : {name_last:1}}).fetch();
    }
});
