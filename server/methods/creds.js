Meteor.methods({
    getConfigSiteUrl : function(){
        var siteConfig = journalConfig.findOne({}, {fields: {journal : 1}});
        if(siteConfig){
            return siteConfig.journal.url;
        }
    },
    getConfigSenderEmail : function(){
        var emailConfig = journalConfig.findOne({}, {fields: {email_sender : 1}});
        if(emailConfig){
            return {
                'address' : emailConfig.email_sender.address,
                'pw' : emailConfig.email_sender.pw
            };
        }
    },
    getConfigSubmission : function(){
        var submissionConfig = journalConfig.findOne({}, {fields: {submission : 1}});
        if(submissionConfig){
            return {
                url: submissionConfig.submission.url,
                username: submissionConfig.submission.user,
                password: submissionConfig.submission.pw,
                cookie : submissionConfig.submission.cookie
            };
        }
    },
    getConfigJournal : function(){
        var settings = journalConfig.findOne({}, {fields: {journal : 1}});
        if(settings){
            return settings.journal;
        }
    },
});
