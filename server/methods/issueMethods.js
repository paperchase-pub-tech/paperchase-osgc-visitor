Meteor.methods({
    archive: function(articleIssueId){
        //combine vol and publish collections
        var journalData,
            journal,
            assetUrl,
            publishedIssues,
            volumesList;
        var issuesList = [],
            issuesObj = {};

        journalData = journalConfig.findOne({});
        journal = journalData.journal.short_name;
        assetUrl =  journalData.assets;

        publishedIssues = publish.find({publication_type: 'issue', 'data.display': true},{}).fetch();
        if (publishedIssues && publishedIssues.length > 0) {
            publishedIssues.forEach(function(issue){
                issuesList.push(issue.data);
            });
        }

        volumesList = volumes.find({},{sort : {volume:-1}}).fetch();

        if (journalData.s3 && journalData.s3.domain && journalData.s3.bucket + journalData.s3.folders && journalData.s3.folders.issues && journalData.s3.folders.issues.covers_optimized) {
            optimizedUrlPath = journalData.s3.domain + journalData.s3.bucket  + '/' + journalData.s3.folders.issues.covers_optimized + '/';
        } else {
            console.error('S3 optimized path not in the database.');
        }

        for(var i=0; i<issuesList.length; i++){
            issuesObj[issuesList[i]._id] = issuesList[i];
        }

        for(var v=0; v < volumesList.length; v++){
            volumesList[v].issues_data = [];
            volumesList[v].year = null;
            var volumeIssues = volumesList[v].issues;
            if(volumeIssues !== undefined) {
                for(var vi=0; vi < volumeIssues.length; vi++){
                    var issueMongoId = volumeIssues[vi];
                    if(issuesObj[issueMongoId]){
                        // add cover path to issue
                        if(issuesObj[issueMongoId].cover){
                            // console.log(issueMongoId);
                            issuesObj[issueMongoId].coverPath = Meteor.issue.coverPath(assetUrl,issuesObj[issueMongoId].cover);
                        }


                        if (issuesObj[issueMongoId].optimized && issuesObj[issueMongoId].optimized_file && issuesObj[issueMongoId].optimized_sizes){
                            issuesObj[issueMongoId].optimized_urls = {};
                            for (var size in issuesObj[issueMongoId].optimized_sizes) {
                                issuesObj[issueMongoId].optimized_urls[size] = optimizedUrlPath + size + '/' + issuesObj[issueMongoId].optimized_file ;
                            }
                        }

                        volumesList[v].issues_data.push(issuesObj[issueMongoId]);

                        if (!volumesList[v].year && issuesObj[issueMongoId].pub_date) {
                            volumesList[v].year = new Date(issuesObj[issueMongoId].pub_date).getFullYear();
                        }
                    }
                }
            }
        }
        return volumesList;
    },
    getDisplayArticlesByIssueId: function(issueId){
        var fut = new future();
        var issueArticles = articles.find({'issue_id' : issueId, display: true},{sort : {page_start:1}}).fetch();
        issueArticles = Meteor.organize.groupArticles(issueArticles);

        fut.return(issueArticles);

        try {
            return fut.wait();
        }
        catch(error) {
            throw new Meteor.Error(error);
        }
    },
    getCurrentIssue: function(){
        var fut = new future();
        var result, issueData;
        var current = issues.findOne({ current: true});

        if (current) {
            issueData = publish.findOne({'publication_type':'issue', 'volume': parseInt(current.volume), 'issue_linkable': current.issue});

            if (issueData && issueData.data) {
                result = issueData.data;
                result.params = Meteor.issue.createIssueParam(issueData.data.volume, issueData.data.issue);
            }

            return result;
        }

        try {
            return fut.wait();
        }
        catch(error) {
            throw new Meteor.Error(error);
        }
    }
});
