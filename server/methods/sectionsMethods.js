// These are for organizing lists of papers in section
Meteor.methods({
    preprocessSectionArticles: function(articles){
        // console.log('..preprocessSectionArticles');console.log(articles);
        var fut = new future();
        // get assets
        for(var i=0 ; i< articles.length ; i++){
            // console.log(articles[i]['_id']);
            articles[i].assets = Meteor.call('availableAssests', articles[i]['_id']);
            if(i == parseInt(articles.length - 1)){
                // console.log(articles);
                articles = Meteor.organize.groupArticles(articles);
                fut['return'](articles);
            }
        }
        return fut.wait();
    },
    getVisibleSections: function() {
        var sects,
        journal,
        assetUrl;

        sects = sections.find({display: true}).fetch();
        journal = journalConfig.findOne({}).journal.short_name;
        assetUrl =  journalConfig.findOne().assets;
        for(var i=0; i < sects.length; i++) {
            var id = sects[i]._id;

            sects[i].coverPath = assetUrl+'special_collections_covers/'+id+'.png';

            sects[i].pdfPath = assetUrl+'special_collections_pdf/'+id+'.pdf';
            sects[i].azw3Path = assetUrl+'special_collections_azw3/'+id+'.azw3';
            sects[i].epubPath = assetUrl+'special_collections_epub/'+id+'.epub';
            sects[i].mobiPath = assetUrl+'special_collections_mobi/'+id+'.mobi';

        }

        return sects;
    },
    getSectionByDashName: function(dashName) {
        var section,
        journal,
        assetUrl;

        section = sections.findOne({"dash_name":dashName});
        
        return section;
    },
    getSectionArticlesByDashName: function(dashName) {
        var section = sections.findOne({'dash_name' : dashName});
        var arts = articles.find({section:section._id}).fetch();
        for(var i=0; i < arts.length; i++) {
            arts[i].files = Meteor.article.linkFiles(arts[i].files, arts[i]._id);
        }

        return arts;
    },
    getSpecialSectionArticlesByDashName: function(dashName) {
        // sort articles descending vol/iss with advance at top
        var sectionAdvance = [];
        var sectionArticles = [];
        var section = sections.findOne({'dash_name' : dashName});
        var arts = articles.find({section:section._id}, {sort: {volume:-1}}).fetch();
        for(var i=0; i < arts.length; i++) {
            arts[i].files = Meteor.article.linkFiles(arts[i].files, arts[i]._id);

            if (arts[i].issue_id) {
                sectionArticles.push(arts[i]);
            } else {
                sectionAdvance.push(arts[i]);
            }
        }

        sectionArticles.sort(function(a, b){
            return parseFloat(b.volume) - parseFloat(a.volume) || parseFloat(b.issue) - parseFloat(a.issue);
        });

        var result = sectionAdvance.concat(sectionArticles);

        return result;
    }
});
