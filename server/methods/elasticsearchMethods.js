Meteor.methods({
    search: function(query) {
        var mongoClient = Meteor.npmRequire('mongodb').MongoClient;
        var esClient = Meteor.npmRequire('elasticsearch').Client({
            host: Meteor.settings.elasticsearch
        });

        function getArticles(ids) {
            return mongoClient.connect(Meteor.settings.mongodb)
            .then(function(db) {
                var output = db.collection('articles').find({
                    _id: {
                        $in: ids
                    }
                }).toArray();

                db.close();

                return output;
            });
        }

        function queryElasticsearch(query) {
            var must = [];

            if (query.general) {
                must.push({
                    multi_match: {
                        query: query.general,
                        fields: ['title', 'abstract', 'authors', 'keywords']
                    }
                });
            }

            if (query.title) {
                must.push({
                    match: {
                        title: {
                            query: query.title,
                            operator: 'and'
                        }
                    }
                });
            }

            if (query.abstract) {
                must.push({
                    match: {
                        abstract: {
                            query: query.abstract,
                            operator: 'and'
                        }
                    }
                });
            }

            if (query.authors) {
                must.push({
                    match: {
                        authors: {
                            query: query.authors,
                            operator: 'and'
                        }
                    }
                });
            }

            if (query.keywords) {
                must.push({
                    match: {
                        keywords: {
                            query: query.keywords,
                            operator: 'and'
                        }
                    }
                });
            }


            return new Promise(function(resolve, reject) {
                var index = (query.agingSearch ? 'aging,' : '') + (query.oncotargetSearch ? 'oncotarget,' : '') + (query.genesandcancerSearch ? 'genesandcancer,' : '') + (query.oncoscienceSearch ? 'oncoscience,' : '');
                if (index !== '') {
                    index = index.slice(0, -1);
                }

                esClient.search({
                    size: 1000,
                    index: index,
                    body: {
                        query: {
                            bool: {
                                must: must
                            }
                        }
                    }
                }, function(err, results) {
                    err ? reject(err) : resolve(results);
                });
            });
        }

        function search(query, cb) {
            return queryElasticsearch(query)
                .then(function(results) {
                    var hits = results.hits.hits.map(function(result) {
                        return result;
                    });

                    return hits;
            })
            .then(function(results) {
              cb(null, results);
            })
            .catch(cb);
        }

        var wrappedSearch = Meteor.wrapAsync(search);
        return wrappedSearch(query);
    }
});
