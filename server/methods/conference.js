Meteor.methods({
    emailConferenceRequest: function(formData){
        var fut = new future();
        var journal = journalConfig.findOne();
        var emailUserData = {};
        var emailAdminData = {};
        var formDataForEmail = '';
        if (journal && journal.site.conference && journal.site.conference.email) {
            // For both user and staff email (form data in html)
            formDataForEmail += '<br/><b>Journal</b>: ' + journal.journal.name;
            formDataForEmail += '<br/><b>First Name</b>: ' + formData.first_name;
            formDataForEmail += '<br/><b>Last Name</b>: ' + formData.last_name;
            formDataForEmail += '<br/><b>Conference Title</b>: ' + formData.conference_title;
            formDataForEmail += '<br/><b>Organization</b>: ' + formData.organization;
            formDataForEmail += '<br/><b>Email</b>: ' + formData.email;
            formDataForEmail += '<br/><b>Phone</b>: ' + formData.phone;
            formDataForEmail += '<br/><b>Conference Topic</b>: ' + formData.conference_topic;
            formDataForEmail += '<br/><b>Conference Start Date</b>: ' + formData.conference_date_start;
            formDataForEmail += '<br/><b>Conference End Date</b>: ' + formData.conference_date_end;
            formDataForEmail += '<br/><b>Description</b>: ' + formData.conf_description;

            // Staff Email
            emailAdminData.to = journal.site.conference.email;
            emailAdminData.subject = 'New Conference Request';
            emailAdminData.html = formDataForEmail;

            // Email staff
            Meteor.call('email', emailAdminData, function(errorAdmin, resultAdmin){
                if (errorAdmin) {
                    console.error('Could not send email admin.', errorAdmin);
                    fut.throw(error);
                } else if (resultAdmin){
                    fut.return(true);
                } else {
                    fut.throw('Cannot send admin email');
                }
            });

        } else {
            console.error('No email in settings file to send conference request to.');
            fut.throw('No email in settings file to send conference request to.');
        }

        try {
            return fut.wait();
        }
        catch(error) {
            throw new Meteor.Error(error);
        }
    }
});
