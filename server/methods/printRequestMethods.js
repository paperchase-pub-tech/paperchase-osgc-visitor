Meteor.methods({
    sendPrintRequestEmail: function(text) {
        var journal = journalConfig.findOne();
        if (journal && journal.print_request && journal.print_request.email && journal.email_sender && journal.email_sender.address) {
            this.unblock();
            Email.send({
                'to' : journal.print_request.email,
                'from' : journal.email_sender.address,
                'subject' : "Request for Reprint",
                'html': text
            });
            return true;
        } else {
            return false;
        }
    }
});
