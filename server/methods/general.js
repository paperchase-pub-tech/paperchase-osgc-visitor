Meteor.methods({
    email: function(emailData){
        var fut = new future();
        var from = Meteor.settings.public && Meteor.settings.public.journal && Meteor.settings.public.journal.name ? Meteor.settings.public.journal.name : '';
        if (Meteor.settings.public && Meteor.settings.public.journal && Meteor.settings.public.journal.nameExtra) {
            from += ' ' + Meteor.settings.public.journal.nameExtra;
        }
        Meteor.call('getConfigSenderEmail', function(error, emailInfo){
            if (error) {
                console.error('Cannot send email', error);
            } else if (emailInfo) {
                if (emailInfo && emailInfo.address && emailData.to && emailData.html) {
                    from += ' <' + emailInfo.address + '>';
                    Email.send({
                        'to' : emailData.to,
                        'from' : from,
                        'subject' : emailData.subject,
                        'html': emailData.html
                    });

                    fut.return(true);
                } else {
                    console.error('Cannot send email.', emailData);
                }
            }
        });

        try {
            return fut.wait();
        }
        catch(error) {
            throw new Meteor.Error(error);
        }
    }
});
