// Config
if (Meteor.isServer) {
    WebApp.connectHandlers.use(function(req, res, next) {
        res.setHeader("Access-Control-Allow-Origin", "*");
        return next();
    });
}

var subs = new SubsManager();

if (Meteor.isClient) {
    subs.subscribe('articleTypes');
    subs.subscribe('sectionsVisible');
    subs.subscribe('interviews');
}

Router.configure({
    loadingTemplate: 'Loading',
    trackPageView: true
});

Meteor.startup(function () {
    // Email ---------
    if (Meteor.isServer) {
        Meteor.call('getConfigSenderEmail', function(error, emailSettings){
            if (emailSettings && emailSettings.address && emailSettings.pw) {
                var connection = 'smtp://' + encodeURIComponent(emailSettings.address) +':' + encodeURIComponent(emailSettings.pw) + '@smtp.mailgun.org:587/';
                process.env.MAIL_URL = connection;
            }
        });
    }

    if (Meteor.isClient) {
        // Altmetric ---------
        Session.set('altmetric-ready', false);
        $.getScript('https://d1bxh8uas1mnw7.cloudfront.net/assets/embed.js', function(a, b, c){
            if(b == 'success') {
                Session.set('altmetric-ready', true);
            }
        });

        // Site Settings and Altmetric ------------------------
        subs.subscribe('journalConfig', function(){
            Session.set('journal', journalConfig.findOne());

            // Altmetric
            // Get top articles
            var journal = Session.get('journal');
            var topNumber = journal && journal.altmetric && journal.altmetric.total_top ? journal.altmetric.total_top : 50;
            Meteor.call('getAltmetricTop', topNumber, function(altmetricError, altmetricResult){
                if (altmetricError) {
                    console.error('altmetricError', altmetricError);
                } else if (altmetricResult) {
                    Session.set('altmetric-count', altmetricResult.length);
                    Session.set('altmetric-top', altmetricResult);
                }
            });

            // Search --------

            if (journal && journal.elasticsearch && journal.elasticsearch.primaryIndex) {
                Meteor.search.setSearchArg(journal.elasticsearch.primaryIndex + 'Search', true);
            }
        });

        Meteor.call('getListWithData', 'sections', function(error,result){
            if(result){
                Session.set('navSection',result);
            }
        });

        // Current Issue ----------
        var currentData;
        Meteor.call('getCurrentIssue', function(error, currentData){
            if (error) {
                console.error('could not get current issue', error);
            } else if (currentData) {
                Session.set('current', currentData);
            }
        });

        // SEO ---------
        SEO.config({
            auto: {
                twitter: false,
                og: false
            }
        });

        // Latest Interview
        Meteor.call('latestInterview', function(errorInterview, resultInterview){
            if (errorInterview) {
                console.error('latestInterview', errorInterview);
            } else if(resultInterview) {
                Session.set('latest-interview', resultInterview);
            }
        });

        // Editorial Board ------
        Meteor.call('getEic', function(errorEic, resultEic){
            if (errorEic) {
                console.error('getEic', errorEic);
            } else if(resultEic) {
                Session.set('edBoardEic', resultEic);
            }
        });
        Meteor.call('getBoardMinusEic', function(errorBoard, resultBoard){
            if (errorBoard) {
                console.error('getBoardMinusEic', errorBoard);
            } else if(resultBoard) {
                Session.set('edBoardBoard', resultBoard);
            }
        });
        Meteor.call('getBoardNew', function(errorBoardNew, resultBoardNew){
            if (errorBoardNew) {
                console.error('getBoardNew', errorBoardNew);
            } else if(resultBoardNew) {
                Session.set('edBoardNew', resultBoardNew);
            }
        });
    }
});

// Image Proxy
// ---------
if (Meteor.isServer) {
    var journal = journalConfig.findOne({});
    WebApp.connectHandlers.use(function(req, res, next) {
        // via @satyavh on Medium
        var img, queryData, request, url, x;
        request = Meteor.npmRequire('request');
        url = Meteor.npmRequire('url');
        if (url.parse(req.url, true).pathname !== '/img') {
            return next();
        }
        queryData = url.parse(req.url, true).query;

        if (!((queryData.img !== null) && (queryData.user !== null))) {
            return next();
        }

        if (queryData.size) {
            img = journal.s3.domain + journal.s3.bucket + '/' + journal.s3.folders.article.figures_optimized + '/' + queryData.size + '/' + queryData.img;
        } else {
            img = journal.assets_figures + '/' + queryData.img;
        }

        x = request(img);
        return req.pipe(x).pipe(res);
    });
}
